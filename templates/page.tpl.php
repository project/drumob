<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<header id="header-navbar" role="banner" class="container-fluid navbar navbar-default navbar-fixed-top nav-justified no-gutters">
<div class="row no-gutters">
    <div id="left-header-button" class ="col-xs-2 text-left">
        <?php if (!(drupal_is_front_page())&&(!empty($header_buttons['left']))): ?>
          <!-- placeholder for the left header button, moved here with jquery from the opened node -->
                <?php print render($header_buttons['left']); ?>
        <?php endif; ?>
     </div>
    <div class="col-xs-8">
        <?php if ($logo): ?>
              <a class=" " href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                   <img src="<?php print $logo; ?>" class ="header-logo-image" alt="<?php print t('Home'); ?>" />
              </a>
        <?php endif; ?>
    </div>
    <div id="right-header-button" class = "col-xs-2 " >
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas="">
          <i class="icon-bar"></i>
          <i class="icon-bar"></i>
          <i class="icon-bar"></i>
        </button>
      </div>
        <nav role="navigation">
          <div class="navmenu navmenu-default navmenu-fixed-right offcanvas-sm offcanvas-md offcanvas-lg" id = "main-menu-offcanvas" style="padding: 5px;">
            <button type="button" class="navmenu-close-button" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas="">
            <i class="fa fa-times" aria-hidden="true"></i>

        </button>
    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
            <?php if (!empty($page['user_info_block'])): ?>
              <?php print render($page['user_info_block']); ?>
            <?php endif; ?>

      <?php if (!empty($page['mobile_visible_menu'])): ?>
              <div class="visible-xs visible-sm">
                  <?php print render($page['mobile_visible_menu']); ?>
              </div>
            <?php endif; ?>
            <?php if (!empty($page['desktop_visible_menu'])): ?>
        <div class="hidden-xs hidden-sm">
                <?php print render($page['desktop_visible_menu']); ?>
              </div>
            <?php endif; ?>
            <?php if (!empty($page['navigation'])): ?>
              <?php print render($page['navigation']); ?>
            <?php endif; ?>
          </div>
        </nav>
    <?php endif; ?>

</div> <!-- end of header row -->
</header>

<div class="main-container container" >


  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->

  <div class="row" >

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?> >
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>


        <?php if ((!empty($title))&&($variables['show_node_heading'])): ?>
          <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>

      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>
  </div>
</div>
<footer id="footer-navbar" role="banner" class="navbar navbar-default navbar-fixed-bottom <?php print render($footer_mobile_visibility) ?> ">
  <?php print render($page['footer']); ?>
      <div class="container">
        <div class="navbar-footer">
          <div id="footer-button-group" class="btn-group-justified">
            <!-- footer buttons, loaded from the template -->
            <?php if (!empty($footer_buttons[1])): ?>
                <?php print render($footer_buttons[1]); ?>
            <?php endif; ?>
            <?php if (!empty($footer_buttons[2])): ?>
                <?php print render($footer_buttons[2]); ?>
            <?php endif; ?>
            <?php if (!empty($footer_buttons['3'])): ?>
                <?php print render($footer_buttons['3']); ?>
            <?php endif; ?>
             <?php if (!empty($footer_buttons['4'])): ?>
                <?php print render($footer_buttons['4']); ?>
            <?php endif; ?>
             <?php if (!empty($footer_buttons['5'])): ?>
                <?php print render($footer_buttons['5']); ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
</footer>
