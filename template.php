<?php

/**
 * @file
 * template.php
 */

/**
 * Gets the html for the left header link from the theme settings.
 *
 * So that it is loaded into the cache.
 */
function drumob_get_right_link_html() {
  $link_icon = theme_get_setting('right_link_icon');
  $link_title = theme_get_setting('right_link_title');
  $link_href = theme_get_setting('right_link_href');
  $output = '';
  if ((!empty($link_icon)) || (!empty($link_title)) || (!empty($link_href))) {
    $output = l('<i class="linkicon__icon fa ' . $link_icon . '"aria-hidden="true"></i><br><span class="linkicon__text">' . $link_title . '</span>', $link_href, array('attributes' => array('class' => array('btn btn-default navbar-btn')), 'html' => TRUE));
  }
  return $output;
}

/**
 * Gets the html for the right back header link from the theme settings.
 *
 * So that it is loaded into the cache.
 */
function drumob_get_back_link_html() {
  $link_icon = theme_get_setting('back_link_icon');
  $link_title = theme_get_setting('back_link_title');
  $output = '';
  // Generate the backlink with javascript history.
  if ((!empty($link_icon)) || (!empty($link_title))) {
    $output = l('<i class="linkicon__icon fa ' . $link_icon . '"aria-hidden="true"></i><span class="linkicon__text">' . $link_title . '</span>', 'javascript:history.back()', array('attributes' => array('class' => array('btn btn-default navbar-btn')), 'html' => TRUE, 'external' => TRUE));
  }
  return $output;
}

/**
 * Gets the html for one footer link from the theme settings.
 *
 * So that it is loaded into the cache.
 */
function drumob_get_footer_link_html($button_number) {
  $link_icon = theme_get_setting('link_icon_' . $button_number);
  $link_title = theme_get_setting('link_title_' . $button_number);
  $link_href = theme_get_setting('link_href_' . $button_number);
  $output = '';
  if ((!empty($link_icon)) || (!empty($link_title)) || (!empty($link_href))) {
    $output = l('<i class="linkicon__icon fa ' . $link_icon . '"aria-hidden="true"></i><br><span class="linkicon__text">' . $link_title . '</span>', $link_href, array('attributes' => array('class' => array('btn btn-default navbar-btn')), 'html' => TRUE));
  }
  return $output;
}

/**
 * Implements template_process_html().
 *
 * Override or insert variables into the page template for HTML output.
 */
function drumob_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Implements template_process_page().
 */
function drumob_process_page(&$variables, $hook) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
}

/**
 * Add / modify variables before the page renders.
 */
function drumob_preprocess_page(&$vars) {
  // Adds the set in the theme variable tooöbar buttons
  // to the page render variables
  // Header left and right:
  $vars['header_buttons']['left'] = drumob_get_back_link_html();
  // $vars['header_buttons']['right'] = drumob_get_right_link_html();
  // footer 1-5:
  $vars['footer_buttons'][1] = drumob_get_footer_link_html(1);
  $vars['footer_buttons'][2] = drumob_get_footer_link_html(2);
  $vars['footer_buttons'][3] = drumob_get_footer_link_html(3);
  $vars['footer_buttons'][4] = drumob_get_footer_link_html(4);
  $vars['footer_buttons'][5] = drumob_get_footer_link_html(5);
  $vars['show_node_heading'] = theme_get_setting('show_node_heading');
  // Add the loading animation to the css inline.
  // So that it can be passed from the settings.
  if (theme_get_setting('show_loading_animation')) {

    if (theme_get_setting('animation_icon')) {
      $selected_animation_icon = theme_get_setting('animation_icon');
      drupal_add_css(('body.loading{background: url("' . path_to_theme() . '/assets/img/' . $selected_animation_icon . '.gif' . '") no-repeat 50% 20%}'), 'inline');
    }
    else {
      drupal_add_css(('body.loading{background: url("' . path_to_theme() . '/assets/img/loading1.gif' . '") no-repeat 50% 20%}'), 'inline');
    }
  }
  // Add the navmenu font size to the css inline.
  // So that it can be passed from the settings.
  if (theme_get_setting('navmenu_font_size')) {
    $nmfs_placeholder = theme_get_setting('navmenu_font_size');
    if (!empty($nmfs_placeholder)) {
      drupal_add_css(('.region-navigation a{font-size: ' . $nmfs_placeholder . '}'), 'inline');
    }
  }

  if (theme_get_setting('show_footer_on_mobile_only')) {
    $vars['footer_mobile_visibility'] = 'visible-xs visible-sm';
  }
  // Custom content type page template
  // Renders a new page template to the list of templates used if it exists.
  if (isset($vars['node']->type)) {
    // This code looks for any page--custom_content_type.tpl.php page.
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
}
